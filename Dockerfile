FROM registry.gitlab.com/forestcitylabs/ops/php-base:latest

# Create a small shell script for executing console commands.
RUN curl -L https://github.com/fabpot/local-php-security-checker/releases/download/v1.0.0/local-php-security-checker_1.0.0_linux_amd64 -o /usr/local/bin/security-checker
RUN chmod a+x /usr/local/bin/security-checker
